<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Marcas extends CI_Controller {
	function __construct(){
		parent::__construct();
		//Carga de Modelos
		$this->load->model("Marca");
	}

	public function index(){
		$data['view'] = 'Marcas/index';
		$data['tituloPagina'] = "Módulo de Marcas";
		$data['output'] = '';
		$this->load->view('Modulos/principal',$data);
	}

	public function lista(){
		$data['lista'] = $this->Marca->buscarMarcas();
		$this->load->view("Marcas/lista", $data);
	}

	public function gestionRegistro(){
		$idMarca = $this->input->post("idMarca");
		
		$data = array("nombre" => $this->input->post("nombreMarca"),
				  	  "origen" => $this->input->post("origenMarca")
					);
		if($idMarca > 0){
			//editar
			echo json_encode('e|'.$this->Marca->editarRegistro($idMarca, $data));
		}
		else{
			//insertar
			echo json_encode('i|'.$this->Marca->insertarRegistro($data));
		}
	}

	public function editarRegistro(){
		$idMarca = $this->input->post("id");
		$marca = $this->Marca->buscarRegistroPorID($idMarca);
		print_r(json_encode($marca));
	}

	public function eliminarRegistro(){
		$idMarca = $this->input->post("id");
		$resultado = $this->Marca->eliminarRegistro($idMarca);
		if($resultado){
			echo json_encode(true);
		}
		else{
			echo json_encode(false);
		}
	}

}

?>
