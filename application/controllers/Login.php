<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CREADO POR PATRICIO RENGIFO
 */
class Login extends CI_Controller
{
	
	function __construct(){
		parent :: __construct();

		//CARGA DE MODELOS
		$this ->load->model("Usuario");
	}
	public function index (){
		//CARGAR UNA VISTA
		$this ->load->view("Login/index");
	
	}
	public function validarIngreso(){
		$emailUsuario = $this->input->post("emailUsuario");
		$claveUsurio = $this->security->xss_clean ($this->input->post ("claveUsuario"));
        //echo $this->encryption->encrypt("123");
        //md5, sha1()
        $usuario =$this->Usuario->buscarUsuarioPorEmail($emailUsuario);
        //$clave =$this->encryption->decrypt($usuario->claveUsuario);
        //echo $clave;
        //$clave = $usuario->claveUsuario;
        
        // print_r($usuario);

        if(is_object($usuario)){
        	//COMPARAR LA CONTRASEÑA
        	$clave =$this->encryption->decrypt($usuario->claveUsuario);

        	if($claveUsurio == $clave ){
        		//Ingreso
        		$data = array("nombreUsuario" => $usuario->nombreUsuario,
        			        "apellidoUsuario" => $usuario->apellidoUsuario, 
        			        "emailUsuario" => $usuario->emailUsuario
        			       );
        		$this->session->set_userdata($data);
        		redirect("Modulos/escritorio");
        	}
        	else{
        		$data["mensaje"] = "Clave Incorrecta";
        	    $this->load->view("Login/index", $data);
        	}
        }
        else {
          $data["mensaje"] = "Usuario Incorrecto";
          $this->load->view("Login/index", $data);
	}
}
public function CerrarSesion(){
	echo json_encode($this->session->sess_destroy());

}
}