<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Profesores extends CI_Controller {
	function __construct(){
		parent::__construct();
		//Carga de Modelos
		$this->load->model("Profesor");
	}

	public function index(){
		$data['view'] = 'Profesores/index';
		$data['tituloPagina'] = "Módulo de Profesores";
		$data['output'] = '';
		$this->load->view('Modulos/principal',$data);
	}

	public function lista(){
		$data['lista'] = $this->Profesor->buscarProfesor();
		$this->load->view("Profesores/lista", $data);
	}

	public function gestionRegistro(){
		$idProfesor = $this->input->post("idProfesor");
		
		$data = array("nombre" => $this->input->post("nombreProfesor")
					);
		if($idProfesor > 0){
			//editar
			echo json_encode('e|'.$this->Profesor->editarRegistro($idProfesor, $data));
		}
		else{
			//insertar
			echo json_encode('i|'.$this->Profesor->insertarRegistro($data));
		}
	}

	public function editarRegistro(){
		$idProfesor = $this->input->post("id");
		$profesor = $this->Profesor->buscarRegistroPorID($idProfesor);
		print_r(json_encode($profesor));
	}

	public function eliminarRegistro(){
		$idProfesor = $this->input->post("id");
		$resultado = $this->Profesor->eliminarRegistro($idProfesor);
		if($resultado){
			echo json_encode(true);
		}
		else{
			echo json_encode(false);
		}
	}

}

?>
