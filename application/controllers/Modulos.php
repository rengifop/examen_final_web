<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Creado por: Marcelo Quimbita
 */
class Modulos extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		//Carga de Modelos
	}

	public function escritorio(){
		//Cargar una vista
		$data['view'] = 'Modulos/escritorio';
		$data['output'] = '';
		$this->load->view('Modulos/principal',$data);	
	}


}