<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Electrodomesticos extends CI_Controller {
	function __construct(){
		parent::__construct();
		//Modelo cvlave foránea
		$this->load->model("Marcas");
		//Modelo clave primaria
		$this->load->model("Electrodomesticos");
	}

	public function index(){
		$data['view'] = 'Electrodomesticos/index';
		$data['tituloPagina'] = "Módulo de Electrodomesticos";
		//Buscar valores de la tabla de la clave foránea
		$data['marca'] = $this->Marca->buscarMarcas();
		$data['output'] = '';
		$this->load->view('Modulos/principal',$data);
	}

	public function lista(){
		$data['lista'] = $this->Electrodomestico->buscarElectrodomesticos();
		$this->load->view("Electrodomesticos/lista", $data);
	}

	public function gestionRegistro(){
		$idElectrodomestico = $this->input->post("idElectrodomestico");
		
		$data = array("marca_id" => $this->input->post("idMarca"),
				  	  "nombre" => $this->input->post("nombreElectrodomestico"),
				  	  "fabricacion" => $this->input->post("fabricacionElectrodomestico"),
				  	  "fabricacion" => $this->input->post("fabricacionElectrodomestico"),
				  	  "estado" => $this->input->post("estadoElectrodomestico"),
				  	  "color" => $this->input->post("colorElectrodomestico"),
				  	  "estado" => $this->input->post("estadoElectrodomestico"),
				  	  
					);
		if($idElectrodomestico > 0){
			//editar
			echo json_encode('e|'.$this->idElectrodomestico->editarRegistro($idElectrodomestico, $data));
		}
		else{
			//insertar
			echo json_encode('i|'.$this->Electrodomestico->insertarRegistro($data));
		}
	}

	public function editarRegistro(){
		$idElectrodomestico = $this->input->post("id");
		$marca = $this->Electrodomestico->buscarRegistroPorID($idElectrodomestico);
		print_r(json_encode($marca));
	}

	public function eliminarRegistro(){
		$idElectrodomestico = $this->input->post("id");
		$resultado = $this->Electrodomestico->eliminarRegistro($idElectrodomestico);
		if($resultado){
			echo json_encode(true);
		}
		else{
			echo json_encode(false);
		}
	}

}

?>
