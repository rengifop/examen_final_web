<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Hospitales extends CI_Controller {
	function __construct(){
		parent::__construct();
		//Carga de Modelos
		$this->load->model("Hospital");
	}

	public function index(){
		$data['view'] = 'Hospitales/index';
		$data['tituloPagina'] = "Módulo de Hospitales";
		$data['output'] = '';
		$this->load->view('Modulos/principal',$data);
	}

	public function lista(){
		$data['lista'] = $this->Hospital->buscarHospital();
		$this->load->view("Hospitales/lista", $data);
	}

	public function gestionRegistro(){
		$idHospital = $this->input->post("idHospital");
		
		$data = array("nombre" => $this->input->post("nombreHospital"),
			          "direccion" => $this->input->post("direccionHospital"),
			          "telefono" => $this->input->post("telefonoFijoHospital")
					);
		if($idHospital > 0){
			//editar
			echo json_encode('e|'.$this->Hospital->editarRegistro($idHospital, $data));
		}
		else{
			//insertar
			echo json_encode('i|'.$this->Hospital->insertarRegistro($data));
		}
	}

	public function editarRegistro(){
		$idHospital = $this->input->post("id");
		$hospital = $this->Hospital->buscarRegistroPorID($idHospital);
		print_r(json_encode($hospital));
	}

	public function eliminarRegistro(){
		$idHospital = $this->input->post("id");
		$resultado = $this->Hospital->eliminarRegistro($idHospital);
		if($resultado){
			echo json_encode(true);
		}
		else{
			echo json_encode(false);
		}
	}

}

?>
