<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CREADO POR PATRICIO RENGIFO
 */
class Usuarios extends CI_Controller
{

	function __construct(){
		parent :: __construct();

		//CARGA DE MODELOS
		$this ->load->model("Usuario");
	}
	public function index(){
		$data['view'] = 'Usuarios/index';
		$data['tituloPagina'] = "Modulos de Usuarios";
		$data['output'] = '';
		$this->load->view('Modulos/principal',$data);
	}
	public function lista(){
		$data['lista'] = $this->Usuario->buscarUsuarios();
		$this->load->view("Usuarios/lista",$data);	
	}
	public function gestionRegistro(){
		$idUsuario = $this->input->post("idUsuario");
		$clave = $this->encryption->encrypt($this->input->post("claveUsuario"));

		$config = [
			"upload_path" => "./application/views/Usuarios/photos",
			"allowed_types" => "jpg|jpeg",
			"file_name" => "img"
		];
		$this->load->library("upload", $config);
		if($this->upload->do_upload("fotoUsuario")){
			$dataFoto = array("upload_data" => $this->upload->data());
			$foto =$dataFoto['upload_data']['file_name'];	
		}
		else{
			$foto ="";
		}

		$data = array("apellido" => $this->input->post("apellidoUsuario"),
			          "nombre" => $this->input->post("nombreUsuario"),
			          "email" => $this->input->post("emailUsuario"),
			          "estado" => $this->input->post("estadoUsuario"),
			          "sexo" => $this->input->post("sexoUsuario"),
			          "telefono_fijo" => $this->input->post("telefonofijoUsuario"),
			          "telefono_movil" => $this->input->post("telefonomovilUsuario"),
			          "clave" => $clave,
			          "foto" => $foto
	);
		if($idUsuario > 0){
			//editar
			echo json_encode('e|'.$this->Usuario->editarRegistro($idUsuario,$data));
			
		}
		else{
           // Insertar
           echo json_encode('i|'.$this->Usuario->insertarRegistro($data));
   
		}
	}
	public function editarRegistro(){
		$idUsuario = $this->input->post("id");
		$usuario = $this->Usuario->buscarRegistroPorID($idUsuario);
		print_r(json_encode($usuario));
	}
	public function eliminarRegistro(){
		$idUsuario = $this->input->post("id");
		$resultado = $this->Usuario->eliminarRegistro($idUsuario);
		if($resultado){
		   echo json_encode(true);
	    }
	    else{
	    	echo json_encode(false);
	    }
	}
}
?>