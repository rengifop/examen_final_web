<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CREADO POR PATRICIO RENGIFO
 */
class Clientes extends CI_Controller
{

	function __construct(){
		parent :: __construct();

		//CARGA DE MODELOS
		$this ->load->model("Cliente");
	}
	public function index(){
		$data['view'] = 'Clientes/index';
		$data['tituloPagina'] = "Modulos de Clientes";
		$data['output'] = '';
		$this->load->view('Modulos/principal',$data);
	}
	public function lista(){
		$data['lista'] = $this->Cliente->buscarClientes();
		$this->load->view("Clientes/lista",$data);	
	}
	public function gestionRegistro(){
		$idCliente = $this->input->post("idCliente");
		
		$data = array("apellido" => $this->input->post("apellidoCliente"),
			          "nombre" => $this->input->post("nombreCliente"),
			          "tipo_identificacion" => $this->input->post("tipoIdentificacionCliente"),
			          "numero_identificacion" => $this->input->post("numeroIdentificacionCliente"),
			          "fecha_nacimiento" => $this->input->post("fechaNacimientoCliente"),
			          "fecha_registro" => $this->input->post("fechaRegistroCliente"),
			          "correo_personal" => $this->input->post("correoPersonalCliente"),
			          "correo_corporativo" => $this->input->post("correoCorporativoCliente"),          
			          "sexo" => $this->input->post("sexoCliente"),
			          "religion" => $this->input->post("religionCliente"),
			          "estado_civil" => $this->input->post("estadoCivilCliente"),
			          "etnia" => $this->input->post("etniaCliente"),
			          "tipo_cliente" => $this->input->post("tipoClienteCliente")
			          
	);
		if($idCliente > 0){
			//editar
			echo json_encode('e|'.$this->Cliente->editarRegistro($idCliente,$data));
			
		}
		else{
           // Insertar
           echo json_encode('i|'.$this->Cliente->insertarRegistro($data));
   
		}
	}
	public function editarRegistro(){
		$idCliente = $this->input->post("id");
		$cliente = $this->Cliente->buscarRegistroPorID($idCliente);
		print_r(json_encode($cliente));
	}
	public function eliminarRegistro(){
		$idCliente = $this->input->post("id");
		$resultado = $this->Cliente->eliminarRegistro($idCliente);
		if($resultado){
		   echo json_encode(true);
	    }
	    else{
	    	echo json_encode(false);
	    }
	}
}
?>