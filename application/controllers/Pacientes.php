<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pacientes extends CI_Controller {
	function __construct(){
		parent::__construct();
		//Modelo cvlave foránea
		$this->load->model("Hospital");
		//Modelo clave primaria
		$this->load->model("Paciente");
	}

	public function index(){
		$data['view'] = 'Pacientes/index';
		$data['tituloPagina'] = "Módulo de Pacientes";
		//Buscar valores de la tabla de la clave foránea
		$data['hospital'] = $this->Hospital->buscarHospital();
		$data['output'] = '';
		$this->load->view('Modulos/principal',$data);
	}

	public function lista(){
		$data['lista'] = $this->Paciente->buscarPacientes();
		$this->load->view("Pacientes/lista", $data);
	}

	public function gestionRegistro(){
		$idPaciente = $this->input->post("idPaciente");
		
		$data = array("hospital_id" => $this->input->post("idHospital"),
				  	  "nombre" => $this->input->post("nombrePaciente"),
				  	  "apellido" => $this->input->post("apellidoPaciente"),
				  	  "direccion" => $this->input->post("direccionPaciente"),
				  	  "telefono_celular" => $this->input->post("telefonoCelularPaciente"),
				  	  "edad" => $this->input->post("edadPaciente")
					);
		if($idPaciente > 0){
			//editar
			echo json_encode('e|'.$this->Paciente->editarRegistro($idPaciente, $data));
		}
		else{
			//insertar
			echo json_encode('i|'.$this->Paciente->insertarRegistro($data));
		}
	}

	public function editarRegistro(){
		$idPaciente = $this->input->post("id");
		$paciente = $this->Paciente->buscarRegistroPorID($idPaciente);
		print_r(json_encode($paciente));
	}

	public function eliminarRegistro(){
		$idPaciente = $this->input->post("id");
		$resultado = $this->Paciente->eliminarRegistro($idPaciente);
		if($resultado){
			echo json_encode(true);
		}
		else{
			echo json_encode(false);
		}
	}

}

?>
