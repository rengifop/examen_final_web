<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ingresos extends CI_Controller {
	function __construct(){
		parent::__construct();
		//Modelo cvlave foránea
		$this->load->model("Articulo");
		//Modelo clave primaria
	 $this->load->model("Ingreso");
	}

	public function index(){
		$data['view'] = 'Ingresos/index';
		$data['tituloPagina'] = "Módulo de Ingresos";
		//Buscar valores de la tabla de la clave foránea
		$data['articulo'] = $this->Articulo->buscarArticulos();
		$data['output'] = '';
		$this->load->view('Modulos/principal',$data);
	}

	public function lista(){
		$data['lista'] = $this->Ingreso->buscarIngresos();
		$this->load->view("Ingresos/lista", $data);
	}

	public function gestionRegistro(){
		$idIngreso = $this->input->post("idIngreso");
		
		$data = array("articulos_id" => $this->input->post("idArticulos"),
				  	  "fecha_ingreso" => $this->input->post("fechaIngreso"),
				  	  "detalle" => $this->input->post("detalleIngreso"),
				  	  "cantidad_ingreso" => $this->input->post("cantidadIngreso"),
				  	  "precio_ingreso" => $this->input->post("precioIngreso"),
				  	  "numero_factura" => $this->input->post("numeroFacturaIngreso")
				  	 
					);
		if($idIngreso > 0){
			//editar
			echo json_encode('e|'.$this->Ingreso->editarRegistro($idIngreso, $data));
		}
		else{
			//insertar
			echo json_encode('i|'.$this->Ingreso->insertarRegistro($data));
		}
	}

	public function editarRegistro(){
		$idIngreso = $this->input->post("id");
		$articulo = $this->Ingreso->buscarRegistroPorID($idIngreso);
		print_r(json_encode($articulo));
	}

	public function eliminarRegistro(){
		$idIngreso = $this->input->post("id");
		$resultado = $this->Ingreso->eliminarRegistro($idIngreso);
		if($resultado){
			echo json_encode(true);
		}
		else{
			echo json_encode(false);
		}
	}

}

?>
