<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Materias extends CI_Controller {
	function __construct(){
		parent::__construct();
		//Modelo cvlave foránea
		$this->load->model("Profesor");
		//Modelo clave primaria
		$this->load->model("Materia");
	}

	public function index(){
		$data['view'] = 'Materias/index';
		$data['tituloPagina'] = "Módulo de Materias";
		//Buscar valores de la tabla de la clave foránea
		$data['profesor'] = $this->Profesor->buscarProfesor();
		$data['output'] = '';
		$this->load->view('Modulos/principal',$data);
	}

	public function lista(){
		$data['lista'] = $this->Materia->buscarMaterias();
		$this->load->view("Materias/lista", $data);
	}

	public function gestionRegistro(){
		$idMateria = $this->input->post("idMateria");
		
		$data = array("profesor_id" => $this->input->post("idProfesor"),
				  	  "nombre" => $this->input->post("nombreMateria"),
				  	  "temas" => $this->input->post("temasMateria"),
				  	  "edicion" => $this->input->post("edicionMateria")
					);
		if($idMateria > 0){
			//editar
			echo json_encode('e|'.$this->Materia->editarRegistro($idMateria, $data));
		}
		else{
			//insertar
			echo json_encode('i|'.$this->Materia->insertarRegistro($data));
		}
	}

	public function editarRegistro(){
		$idMateria = $this->input->post("id");
		$materia = $this->Materia->buscarRegistroPorID($idMateria);
		print_r(json_encode($materia));
	}

	public function eliminarRegistro(){
		$idMateria = $this->input->post("id");
		$resultado = $this->Materia->eliminarRegistro($idMateria);
		if($resultado){
			echo json_encode(true);
		}
		else{
			echo json_encode(false);
		}
	}

}

?>
