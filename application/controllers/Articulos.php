<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Articulos extends CI_Controller {
	function __construct(){
		parent::__construct();
		//Carga de Modelos
		$this->load->model("Articulo");
	}

	public function index(){
		$data['view'] = 'Articulos/index';
		$data['tituloPagina'] = "Módulo de Articulos";
		$data['output'] = '';
		$this->load->view('Modulos/principal',$data);
	}

	public function lista(){
		$data['lista'] = $this->Articulo->buscarArticulos();
		$this->load->view("Articulos/lista", $data);
	}

	public function gestionRegistro(){
		$idArticulo = $this->input->post("idArticulo");

		$config = [
			"upload_path" => "./application/views/Articulos/photos",
			"allowed_types" => "jpg|jpeg",
			"file_name" => "img"
		];
		$this->load->library("upload", $config);
		if($this->upload->do_upload("fotoArticulo")){
			$dataFoto = array("upload_data" => $this->upload->data());
			$foto =$dataFoto['upload_data']['file_name'];	
		}
		else{
			$foto ="";
		}
		

		$data = array("nombre_corto" => $this->input->post("nombreCortoArticulo"),
			          "nombre_completo" => $this->input->post("nombreCompletoArticulo"),
			          "codigo_barra" => $this->input->post("codigoBarrasArticulo"),
			          "fecha_ingreso" => $this->input->post("fechaIngresoArticulo"),
			          "estado" => $this->input->post("estadoArticulo"),
			          "unidad" => $this->input->post("unidadArticulo"),
			          "precio_compra" => $this->input->post("precioCompraArticulo"),
			          "precio_distribuidor" => $this->input->post("precioDistribuidorArticulo"),
			          "precio_publico" => $this->input->post("precioPublicoArticulo"),
			          "tipo" => $this->input->post("tipoArticulo"),
			          "foto" => $foto
					);
		if($idArticulo > 0){
			//editar
			echo json_encode('e|'.$this->Articulo->editarRegistro($idArticulo, $data));
		}
		else{
			//insertar
			echo json_encode('i|'.$this->Articulo->insertarRegistro($data));
		}
	}

	public function editarRegistro(){
		$idArticulo = $this->input->post("id");
		$articulo = $this->Articulo->buscarRegistroPorID($idArticulo);
		print_r(json_encode($articulo));
	}

	public function eliminarRegistro(){
		$idArticulo = $this->input->post("id");
		$resultado = $this->Articulo->eliminarRegistro($idArticulo);
		if($resultado){
			echo json_encode(true);
		}
		else{
			echo json_encode(false);
		}
	}

}

?>
