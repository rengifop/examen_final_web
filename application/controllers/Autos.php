<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Autos extends CI_Controller {
	function __construct(){
		parent::__construct();
		//Modelo cvlave foránea
		$this->load->model("Marca");
		//Modelo clave primaria
		$this->load->model("Auto");
	}

	public function index(){
		$data['view'] = 'Autos/index';
		$data['tituloPagina'] = "Módulo de Autos";
		//Buscar valores de la tabla de la clave foránea
		$data['marca'] = $this->Marca->buscarMarcas();
		$data['output'] = '';
		$this->load->view('Modulos/principal',$data);
	}

	public function lista(){
		$data['lista'] = $this->Auto->buscarAutos();
		$this->load->view("Autos/lista", $data);
	}

	public function gestionRegistro(){
		$idAuto = $this->input->post("idAuto");
		
		$data = array("marca_id" => $this->input->post("idMarca"),
				  	  "nombre" => $this->input->post("nombreAuto"),
				  	  "fabricacion" => $this->input->post("fabricacionAuto"),
				  	  "tipo" => $this->input->post("tipoAuto"),
				  	  "fabricacion" => $this->input->post("fabricacionAuto"),
				  	  "estado" => $this->input->post("estadoAuto"),
				  	  "color" => $this->input->post("colorAuto"),
				  	  "estado" => $this->input->post("estadoAuto"),
				  	  "puerta" => $this->input->post("puertaAuto")
					);
		if($idAuto > 0){
			//editar
			echo json_encode('e|'.$this->Auto->editarRegistro($idAuto, $data));
		}
		else{
			//insertar
			echo json_encode('i|'.$this->Auto->insertarRegistro($data));
		}
	}

	public function editarRegistro(){
		$idAuto = $this->input->post("id");
		$marca = $this->Auto->buscarRegistroPorID($idAuto);
		print_r(json_encode($marca));
	}

	public function eliminarRegistro(){
		$idAuto = $this->input->post("id");
		$resultado = $this->Auto->eliminarRegistro($idAuto);
		if($resultado){
			echo json_encode(true);
		}
		else{
			echo json_encode(false);
		}
	}

}

?>
