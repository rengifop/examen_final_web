<?php if($lista){ ?>

<div class="table-responsive">	
  <table class="table table-bordered">
	<thead>
		<tr>
			<th width="10%">N°</th>
			<th width="20%">Nomina</th>
			<th width="20%">Direccion</th>
			<th width="10%">Telefono Celular</th>
			<th width="10%">Edad</th>
			<th width="12%"></th>
		</tr>
	</thead>
	<tbody>
		<?php $orden = 1; ?>
		<?php foreach ($lista as $dt){ ?>
		<tr>
			<td><?php echo $orden; $orden++; ?></td>
			<td> <?php echo $dt->apellidoPaciente. ' '.$dt->nombrePaciente; ?></td>
			<td><?php echo $dt->direccionPaciente; ?></td>
			<td><?php echo $dt->telefonoCelularPaciente; ?></td>
			<td><?php echo $dt->edadPaciente; ?></td>
			<td>
				<button type="button" class="btn btn-info btn-sm" data-accion="editar" onclick="gestionRegistro(this);" data-id="<?php echo $dt->idPaciente; ?>"><i class="fa fa-pencil"></i></button>
				<button type="button" class="btn btn-danger btn-sm" data-accion="eliminar" onclick="gestionRegistro(this);"data-id="<?php echo $dt->idPaciente; ?>"><i class="fa fa-trash"></i></button>
			</td>
		</tr>
		<?php } ?>
	</tbody>
	<tfoot>
		<tr>
			<th>N°</th>
			<th>Nomina</th>
			<th>Direccion</th>
			<th>Telefono Celular</th>
			<th>Edad</th>
			<th></th>
		</tr>		
	</tfoot>	
  </table>

</div>
<?php }else{ ?>
  <div class="alert alert-danger">
       No existen datos
  </div>
<?php } ?>