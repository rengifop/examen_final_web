<?php if($lista){ ?>

<div class="table-responsive">	
  <table class="table table-bordered">
	<thead>
		<tr>
			<th width="10%">N°</th>
			<th width="20%">Marca</th>
			<th width="20%">Nombre</th>
			<th width="10%">Fabricación</th>
			<th width="15%">Tipo</th>
			<th width="15%">Estado</th>
			<th width="12%"></th>
		</tr>
	</thead>
	<tbody>
		<?php $orden = 1; ?>
		<?php foreach ($lista as $dt){ ?>
		<tr>
			<td><?php echo $orden; $orden++; ?></td>
			<td><?php echo $dt->nombreMarca; ?></td>
			<td><?php echo $dt->nombreAuto; ?></td>
			<td><?php echo $dt->fabricacionAuto; ?></td>
			<td><?php echo $dt->tipoAuto; ?></td>
			<td>
				<?php if($dt->estadoAuto == "B"){ ?>
					<small class="label label-success"><i class="fa fa-smile-o"></i> BUENO</small>
				<?php }else{ ?>	
					<small class="label label-danger"><i class="fa fa-frown-o"></i> MALO</small>
				<?php } ?>
			</td>
			<td>
				<button type="button" class="btn btn-info btn-sm" data-accion="editar" onclick="gestionRegistro(this);" data-id="<?php echo $dt->idAuto; ?>"><i class="fa fa-pencil"></i></button>
				<button type="button" class="btn btn-danger btn-sm" data-accion="eliminar" onclick="gestionRegistro(this);"data-id="<?php echo $dt->idAuto; ?>"><i class="fa fa-trash"></i></button>
			</td>
		</tr>
		<?php } ?>
	</tbody>
	<tfoot>
		<tr>
			<th>N°</th>
			<th>Marca</th>
			<th>Nombre</th>
			<th>Fabricación</th>
			<th>Tipo</th>
			<th>Estado</th>
			<th></th>
		</tr>		
	</tfoot>	
  </table>

</div>
<?php }else{ ?>
  <div class="alert alert-danger">
       No existen datos
  </div>
<?php } ?>