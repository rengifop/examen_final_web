  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/plugins/plantilla/img/pato1.jpg'); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>
              <?php  echo $this ->session->userdata("nombreUsuario")." ".$this ->session->userdata("apellidoUsuario");
              ?> 
          </p>
           <p>
              <?php  echo $this ->session->userdata("nombreCliente")." ".$this ->session->userdata("apellidoCliente");
              ?> 
          </p>
          
          <a href="#"><i class="fa fa-circle text-success"></i> En linea</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Buscar...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENÚ PRINCIPAL</li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Escritorio</span></a></li>
        <li class="treeview" id ="menuOpciones">
          <a href="#">
            <i class="fa fa-dashboard" ></i> <span>Opciones</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
            
          <ul class="treeview-menu"> 

            <li id="menuArticulos"><a href="<?php echo base_url('Articulos/index'); ?>" ><i class="fa fa-circle-o"></i> Articulos </a></li>
            <li id="menuIngresos"><a href="<?php echo base_url('Ingresos/index'); ?>" ><i class="fa fa-circle-o"></i> Ingreso </a></li>
          </ul>
        </li>


        <!-- <ul class="treeview-menu"> 

            <li id="menuUsuarios"><a href="<?php echo base_url('Usuarios/index'); ?>" ><i class="fa fa-circle-o"></i> Usuarios </a></li>
            <li id="menuClientes"><a href="<?php echo base_url('Clientes/index'); ?>" ><i class="fa fa-circle-o"></i> Clientes </a></li>
          </ul>
        </li>
        
          
          <li class="treeview" id="menuMecanico">
          <a href="#">
            <i class="fa fa-car"></i> <span>Autos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="menuMarcas"><a href="<?php echo base_url('Marcas/index'); ?>" ><i class="fa fa-circle-o"></i> Marcas</a></li>
            <li id="menuAutos"><a href="<?php echo base_url('Autos/index'); ?>"><i class="fa fa-circle-o"></i> Autos</a></li>
          </ul>
        </li>
        
        <li class="treeview" id="menuEscuela">
          <a href="#">
            <i class="fa fa-file-text-o"></i> <span>Materia</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="menuProfesor"><a href="<?php echo base_url('Profesores/index'); ?>" ><i class="fa fa-circle-o"></i> Profesor</a></li>
            <li id="menuMaterias"><a href="<?php echo base_url('Materias/index'); ?>"><i class="fa fa-circle-o"></i> Materia</a></li>
          </ul>
        </li>


           <li class="treeview" id="menuSalud">
          <a href="#">
            <i class="fa fa-hospital-o"></i> <span>Hospitales</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="menuHospitales"><a href="<?php echo base_url('Hospitales/index'); ?>" ><i class="fa fa-circle-o"></i> Hospital</a></li>
            <li id="menuPacientes"><a href="<?php echo base_url('Pacientes/index'); ?>"><i class="fa fa-circle-o"></i> Pacientes</a></li>
          </ul>
        </li>

          
<!--        <li class="treeview" id="menuArtefactos">
          <a href="#">
            <i class="fa fa-dashboard"></i> Electrodomésticos<span></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="menuMarcas"><a href="<?php echo base_url('Marcas/index'); ?>" ><i class="fa fa-circle-o"></i>Marcas</a></li>
            <li id="menuElectrodomesticos"><a href="<?php echo base_url('Electrodomesticos/index'); ?>"><i class="fa fa-circle-o"></i> Electrodomesticos</a></li>
          </ul>
        </li>
-->


        <li><a href="#" onclick="CerrarSesion();"><i class="fa fa-close"></i> <span>Salir</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
   <script type="text/javascript">
     function CerrarSesion(){

              Swal.fire({
          title: 'Desea Salir?',
          text: "Terminar la Sesión!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'No',
          confirmButtonText: 'Sí, Salir!'
        }).then((result) => {
          if (result.value) {
            $.ajax({
              url : "<?php echo base_url('Login/CerrarSesion'); ?>"
            }). done(function(){
              location.href = "<?php echo base_url('Login/index') ?>";
            });
          }
          })
             }
   </script>
  
  <!-- =============================================== -->


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
