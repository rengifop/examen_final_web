<?php if($lista){ ?>

<div class="table-responsive">	
  <table class="table table-bordered">
	<thead>
		<tr>
			<th width="10%">N°</th>
			<th width="20%">Fecha Ingreso</th>
			<th width="20%">Detalle</th>
			<th width="10%">Cantidad Ingreso</th>
			<th width="15%">Precio Ingreso</th>
			<th width="15%">Numero Factura</th>
			<th width="12%"></th>
		</tr>
	</thead>
	<tbody>
		<?php $orden = 1; ?>
		<?php foreach ($lista as $dt){ ?>
		<tr>
			<td><?php echo $orden; $orden++; ?></td>
			<td><?php echo $dt->fechaIngreso; ?></td>
			<td><?php echo $dt->detalleIngreso; ?></td>
			<td><?php echo $dt->cantidadIngreso; ?></td>
			<td><?php echo $dt->precioIngreso; ?></td>
			<td><?php echo $dt->numeroFacturaIngreso; ?></td>
			
			
			<td>
				<button type="button" class="btn btn-info btn-sm" data-accion="editar" onclick="gestionRegistro(this);" data-id="<?php echo $dt->idIngreso; ?>"><i class="fa fa-pencil"></i></button>
				<button type="button" class="btn btn-danger btn-sm" data-accion="eliminar" onclick="gestionRegistro(this);"data-id="<?php echo $dt->idIngreso; ?>"><i class="fa fa-trash"></i></button>
			</td>
		</tr>
		<?php } ?>
	</tbody>
	<tfoot>

		<tr>
			<th >N°</th>
			<th >Fecha Ingreso</th>
			<th >Detalle</th>
			<th >Cantidad Ingreso</th>
			<th >Precio Ingreso</th>
			<th >Numero Factura</th>
			<th ></th>
		</tr>

	</tfoot>	
  </table>

</div>
<?php }else{ ?>
  <div class="alert alert-danger">
       No existen datos
  </div>
<?php } ?>