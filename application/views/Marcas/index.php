
<script type="text/javascript">
  $("#menuMecanico").addClass("active");
  $("#menuMarcas").addClass("active");
</script>

      <h1>
        <?php echo $tituloPagina; ?>
        <small>Gestión de marcas</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#"></a></li>
        <li class="active">Marcas</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
             <button type="button" class="btn btn-success" data-accion="insertar" onclick="gestionRegistro(this);"><i class="fa fa-file"></i> Nuevo registro</button> 
          </h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body" id="listadoDatos">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- Modal -->
<div id="modalFormulario" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
     
     <form id="formRegistro">

      <input type="hidden" name="idMarca" id="idMarca">

      <div class="modal-header">
        <h4 id="tituloModal"></h4>
      </div>

      <div class="modal-body">
        
        <div class="row">
          <div class="col-sm-6">
            <label>Nombre</label>
            <input type="text" name="nombreMarca" id="nombreMarca" class="form-control" placeholder="Nombre">
          </div>
          <div class="col-sm-6">
            <label>Origen</label>
            <input type="text" name="origenMarca" id="origenMarca" class="form-control" placeholder="País origen">
          </div>
        </div> 
        
      
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
     </form>

    </div>
  </div>
</div>


  <script type="text/javascript">
    
    listarDatos();

    function listarDatos(){
      
      $("#listadoDatos").load("<?php echo base_url('Marcas/lista') ?>",{},function(responseText, statusText, xhr){
        if(statusText == "success"){
        }
        if(statusText == "error"){
        }
      });
    
    }

    function gestionRegistro(objeto){
      var accion = $(objeto).data("accion");
      switch(accion){
        case 'insertar':
          $("#idMarca").val("");
          $("#formRegistro")[0].reset();
          $("#modalFormulario").modal("show");
          $("#tituloModal").text("Insertar registro");
        break;
        case 'editar':
          var id = $(objeto).data("id");
          editarRegistro(id);
        break;
        case 'eliminar':

          Swal.fire({
            title: 'Eliminar el registro?',
            text: "Se perderán los datos!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'No'
          }).then((result) => {
            if (result.value) {

              var id = $(objeto).data("id"); 
              $.ajax({
                type : "post",
                dataType: "json",
                url: "<?php echo base_url('Marcas/eliminarRegistro'); ?>",
                data: {
                  id : id
                },
                success: function(data){
                  Swal.fire({
                    position: 'top-end',
                    type: 'success',
                    title: 'Registro eliminado exitosamente',
                    showConfirmButton: false,
                    timer: 1500
                  })
                },
                complete:function(){
                  listarDatos();
                },
                error:function(err){
                  Swal.fire({
                    position: 'top-end',
                    type: 'warning',
                    title: 'No se pudo eliminar',
                    showConfirmButton: false,
                    timer: 1500
                  })
                } 
              });

            }
          })

        break;
      } 
    }

    //Validacion de formulario

    $("#formRegistro").validate({
      rules : {
        nombreMarca : {
          required : true
        },
        origenMarca : {
          required : true
        }
      },
      messages : {
        nombreMarca : {
          required : "Ingrese un nombre"
        },
        origenMarca : {
          required : "País de origen"
        }
      },
      submitHandler: function(form) {
        var formData = new FormData($('#formRegistro')[0]);
        $.ajax({
          type : "post",
          url : "<?php echo base_url('Marcas/gestionRegistro'); ?>",
          data : formData,
          async: false,
          cache: false,
          contentType: false,
          processData: false,
          enctype: 'multipart/form-data',
          success:function(data){
            var resultado = data.replace('"', '');
            resultado = resultado.split("|");
            switch(resultado[0]){
              case 'i':
                var mensaje = "creado";
              break;
              case 'e':
                var mensaje = "editado";
              break;
            }
            //igual - done
            Swal.fire({
              position: 'top-end',
              type: 'success',
              title: 'Registro '+mensaje+' exitosamente',
              showConfirmButton: false,
              timer: 1500
            })
            $("#modalFormulario").modal("hide");
          },
          complete:function(){
            // igual a always
            listarDatos();
          },
          error:function(err){
             
          }   
        });
      }
    });

    //Editar registro
    function editarRegistro(id){
      $.ajax({
        type : "post",
        url : "<?php echo base_url('Marcas/editarRegistro'); ?>",
        dataType : 'json',
        data : {
          id : id
        },
        success: function(data){
          $(data).each(function(i, v){
            $("#idMarca").val(v.id);
            $("#nombreMarca").val(v.nombre);
            $("#origenMarca").val(v.origen);
          });

          $("#modalFormulario").modal("show");
          $("#tituloModal").text("Editar registro");
          
        },
        complete:function(){
        },
        error:function(err){
            Swal.fire({
              position: 'top-end',
              type: 'warning',
              title: 'Error al recuperar datos',
              showConfirmButton: false,
              timer: 1500
            })
        }
      });
    }

  </script>