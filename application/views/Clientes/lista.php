<?php if ($lista){ ?>

	<div class="table-responsive">
  <table class="table table-hover">
		
		<thead>
			<tr>
				<th width="10%">Nº</th>
				<th width="20%">Nómina</th>
				<th width="15%">Correo Personal</th>
				<th width="10%">Sexo</th>
				<th width="10%">Religion</th>
				<th width="10%">Etnia</th>
				<th width="10%">Fecha Registro</th>
				<th width="10%"></th>
			</tr>
				
		</thead>	
			<tbody>
				<?php $orden = 1; ?>
				<?php foreach ($lista as $dt){ ?>
				
				<tr>
					<td> <?php echo $orden; $orden++; ?></td>
					<td> <?php echo $dt->apellido. ' '.$dt->nombre; ?></td>
					<td><?php echo $dt->correo_personal; ?></td>
					<td><?php echo $dt->sexo; ?></td>
					<td><?php echo $dt->religion; ?></td>
					<td><?php echo $dt->etnia; ?></td>
					<td><?php echo $dt->fecha_registro; ?></td>

					<td>
						<button type="button" class="btn btn-info btn-sm" data-accion="editar" onclick="gestionRegistro(this);" data-id="<?php echo $dt->id; ?>"> <i class="fa fa-pencil"></i> </button>
						
						<button type="button" class="btn btn-danger btn-sm" data-accion="eliminar"onclick="gestionRegistro(this);" data-id="<?php echo $dt->id; ?>"> <i class="fa fa-trash"></i> </button>
					</td>
				</tr>
				<?php } ?>

			</tbody>
			     <tfoot>
			     	<tr>
			           <th >Nº</th>
				       <th >Nómina</th>
				       <th >Correo Personal</th>
				       <th >Sexo</th>
				       <th >Religion</th>
				       <th >Etnia</th>
				       <th >Fecha Registro</th>
				       <th ></th>
			     	</tr>
				     
			      </tfoot>
		
	 </table>
	</div>
<?php } else { ?>
	<div class="alert alert-danger"> 
      No existe datos
        </div>
<?php } ?>