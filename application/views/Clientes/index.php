<script type="text/javascript">
  $("#menuOpciones").addClass("active");
  $("#menuClientes").addClass("active");
</script>
      <h1>
        <?php echo $tituloPagina; ?>
        <small>Gestion Cliente</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#"></a></li>
        <li class="active">Clientes</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <button type="button" class="btn btn-success" data-accion="insertar" onclick="gestionRegistro(this);"><i class="fa fa-file"></i> Nuevo Cliente</button>
          </h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body" id="listadoDatos">
         
        </div>
        <!-- /.box-body -->
     </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!--Ventana modal-->
<div id="modalCliente" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!--Modal content-->
      <div class="modal-content">

      <form id="formRegistro">
        <input type="hidden" name="idCliente" id="idCliente">
      <div class="modal-header">
        <h4 id="tituloModal"></h4>
      </div>

      <div class="modal-body">

        <div class="row">
          <div class="col-sm-6">
            <label>Apellido</label>
             <input type="text" name="apellidoCliente" id="apellidoCliente" class="form-control" placeholder="Apellidos">
              </div>
                <div class="col-sm-6">
               <label>Nombre</label>
             <input type="text" name="nombreCliente" id="nombreCliente" class="form-control" placeholder="Nombres">
          </div>
        </div>
         
          <br>
         <div class="row">
          <div class="col-sm-6">
          <label>Correo Personal</label>  
           <div class="input-group">
             <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
             <input type="email" name="correoPersonalCliente" id="correoPersonalCliente" class="form-control" placeholder="@Email" >
            </div>
           </div> 

             <div class="col-sm-6">
             <label>Correo Corporativo</label>  
              <div class="input-group">
               <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span>
              <input type="email" name="correoCorporativoCliente" id="correoCorporativoCliente" class="form-control" placeholder="@Email" >
            </div>
           </div> 
        </div>
        

          <br>
          <div class="row">
            <div class="col-sm-6">
              <label>Tipo de Identificación</label>
               <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-address-card"></i></span>
               <select name="tipoIdentificacionCliente" id="tipoIdentificacionCliente"  class="form-control">
                <option value="C">Cédula</option>
                <option value="P">Pasaporte</option>
                <option value="R">Ruc</option>
                <option value="D">DNI</option>
           </select>
          </div> 
           </div>

            <div class="col-sm-6">
              <label>Número de Identificación</label>
               <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-id-card "></i></span>
                <input type="text" name="numeroIdentificacionCliente" id="numeroIdentificacionCliente" class="form-control" placeholder="Número Identificación">
              </div>
             </div> 
         </div>

         <br>
        <div class="row">
          <div class="col-sm-6">
          <label>Fecha de Nacimiento</label>  
           <div class="input-group">
             <span class="input-group-addon"><i class="fa fa-gift"></i></span>
             <input type="date" name="fechaNacimientoCliente"  id="fechaNacimientoCliente"class="
             form-control">
            </div>
           </div> 
            
              <div class="col-sm-6">
                <label>Sexo</label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-venus-mars"></i></span>
                  <select name="sexoCliente" id="sexoCliente" class="form-control">
                    <option value="H">Hombre</option>
                    <option value="M">Mujer</option>  
                  </select>                  
                                
              </div>              
            </div>
        </div>


         <br>
        <div class="row">
          <div class="col-sm-4">
           <label>Estado Civil</label>
           <div class="input-group">
           <span class="input-group-addon"><i class="fa fa-rings "></i></span>
           <select name="estadoCivilCliente" id="estadoCivilCliente"  class="form-control">
             <option value="S">Soltero</option>
             <option value="C">Casado</option>
             <option value="V">Viudo</option>
             <option value="D">Divorciado</option>
           </select>
          </div> 
           </div>

            <div class="col-sm-4">
             <label>Etnia</label>
              <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
               <input type="text" name="etniaCliente" id="etniaCliente" class="form-control" placeholder="Etnia">
            </div>
          </div> 
            
            <div class="col-sm-4">
             <label>Religión</label>
              <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-king"></i></span>
               <input type="text" name="religionCliente" id="religionCliente" class="form-control" placeholder="Religión">
            </div>
          </div> 
        </div>

        <br>
         <div class="row">
          <div class="col-sm-6">
          <label>Fecha de Registro</label>  
           <div class="input-group">
             <span class="input-group-addon"><i class="fa fa-calendar "></i></span>
            <input type="date" name="fechaRegistroCliente"  id="fechaRegistroCliente"class="
             form-control" value="<?php echo date("Y-m-d");?>">
            </div>
           </div> 
        </div>


         <br>
      <div class="modal-footer">
        <button type="submit" id="botonGuardar" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
      </div>

      </form> 
    </div>
  </div>
</div>

  <script type="text/javascript">
    listarDatos();

    function listarDatos(){
      $("#listadoDatos").load("<?php echo base_url('Clientes/lista') ?>",{},function(responseText, statusText, xhr){
        if (statusText == "success") {

        }
        if (statusText == "error") {

        }
      });
    }

    function gestionRegistro(objeto){
      var accion = $(objeto).data("accion");

      switch(accion){
        case 'insertar':
        $("#idCliente").val("");
        $("#formRegistro")[0].reset();
        $("#modalCliente").modal("show");
        $("#tituloModal").text("Insertar Cliente");
        break;
        case 'editar':
        var id =$(objeto).data("id");
        editarRegistro(id);
        break;
        case 'eliminar':

        Swal.fire({
         title: 'Eliminar el registro?',
         text: "Se perderán los Datos!",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         cancelButtonText: 'No',
         confirmButtonText: 'Si!'
         }).then((result) => {
          if (result.value) {
           var id =$(objeto).data("id");
           $.ajax({
            type : "post",
            dataType : "json",
            url : "<?php echo base_url('Clientes/eliminarRegistro') ?>",
            data:{
              id : id
            },
            success: function(data){
              Swal.fire({
           position: 'top-end',
            type: 'success',
             title: 'Cliente eliminado exitosamente',
             showConfirmButton: false,
             timer: 1500
              })
            },
            complete: function(){
              listarDatos();
            },
            error: function(err){
              Swal.fire({
           position: 'top-end',
            type: 'warning',
             title: 'No se pudo eliminar',
             showConfirmButton: false,
             timer: 1500
              })
            }
           });
          }
        })
        break;
      }

    }
    //VALIDACION DEL FORMULARIO

    $("#formRegistro").validate({
      rules :{
        apellidoCliente : {
          required : true
        },
        nombreCliente : {
          required : true
        },
        tipoIdentificacionCliente : {
          required : true
        },
        numeroIdentificacionCliente : {
          required : true
        },
        fechaNacimientoCliente : {
          required : true
        },
        correoPersonalCliente : {
          required : true
        },
        correoCorporativoCliente : {
          required : true
        },
        religionCliente : {
          required : true
        },
        estadoCivilCliente : {
          required : true
        },
        etniaCliente : {
          required : true
        },
        tipoClienteCliente : {
          required : true
        }
      },
      messages : {
        apellidoCliente : {
          required : "Ingrese el Apellido",
          field :"Ingrese solo letras"
        },
        nombreCliente : {
          required : "Ingrese el Nombre",
          field :"Ingrese solo letras"
        },

        tipoIdentificacionCliente : {
          required : "Ingrese Tipo de Id",
          field :"Ingrese solo letras"
        },
        numeroIdentificacionCliente : {
          required : "Ingrese la Ident",
          field :"Ingrese solo letras"
        },
        fechaNacimientoCliente : {
          required : "Ingrese la fecha Nacimiento",
          field :"Ingrese solo letras"
        },
        correoPersonalCliente : {
          required : "Ingrese el Correo Personal",
          field :"Ingrese solo letras"
        },
        correoCorporativoCliente : {
          required : "Ingrese el Correo Corporativo",
          field :"Ingrese solo letras"
        },
        religionCliente : {
          required : "Ingrese la Religión",
          field :"Ingrese solo letras"
        },
        estadoCivilCliente : {
          required : "Ingrese el Estado Civil",
          field :"Ingrese solo letras"
        },
        etniaCliente : {
          required : "Ingrese la Etnia",
          field :"Ingrese solo letras"
         },
        tipoClienteCliente : {
          required : "Ingrese el cliente",
          field :"Ingrese solo letras"
         }
        },
      submitHandler: function(form){
        var formData = new FormData($('#formRegistro')[0]);
        $.ajax({
          type : 'post',
          url : '<?php echo base_url("Clientes/gestionRegistro"); ?>', 
          data : formData, 
          async : false,
          cache: false,
          contentType: false,
          processData: false,
          enctype: "multipar/form-data",
          success:function(data){
            var resultado = data.replace('"','');
            resultado = resultado.split("|");
            switch(resultado[0]){
              case 'i':
              var mensaje = "creado";
              break;
              case 'e':
              var mensaje ="editado";
              break;
            }
            //igual - done
            Swal.fire({
           position: 'top-end',
            type: 'success',
             title: 'Cliente ' +mensaje+ ' exitosamente',
             showConfirmButton: false,
             timer: 1500
              })
            $("#modalCliente").modal("hide");
          },
          complete:function(){
            // igual a always
            listarDatos();
          },
          error:function(err){
            swal("Error!", "No se pudo cerar", "info");
          }
        })

      }
    });

    // EDITAR REGISTRO
    function editarRegistro(id){
      $.ajax({
        type : "post",
        url : "<?php echo base_url("Clientes/editarRegistro"); ?>",
        dataType : "json",
        data : {
          id : id
        },
        success:function(data){ 
          $(data).each(function(i, v){
            
            $("#idCliente").val(v.id);
            $("#apellidoCliente").val(v.apellido);
            $("#nombreCliente").val(v.nombre);
            $("#tipoIdentificacionCliente").val(v.tipo_identificacion);
            $("#numeroIdentificacionCliente").val(v.numero_identificacion);
            $("#fechaNacimientoCliente").val(v.fecha_nacimiento);
            $("#fechaRegistroCliente").val(v.fecha_registro);
            $("#correoPersonalCliente").val(v.correo_personal);
            $("#correoCorporativoCliente").val(v.correo_corporativo);
            $("#sexoCliente").val(v.sexo);
            $("#religionCliente").val(v.religion);
            $("#estadoCivilCliente").val(v.estado_civil);
            $("#etniaCliente").val(v.etnia);
            $("#tipoClienteCliente").val(v.tipo_cliente);
          });

         $("#modalCliente").modal("show");
        $("#tituloModal").text("Editar Cliente"); 
        },
        complete:function(){
          },
          error:function(err){
            Swal.fire({
               position: 'top-end',
               type: 'warning',
               title: 'Error al recuperar datos',
               showConfirmButton: false,
               timer: 1500
            })
          }
      });
    }
  </script>
  

  <!-- /.content-wrapper -->

  

  <!-- /.content-wrapper -->