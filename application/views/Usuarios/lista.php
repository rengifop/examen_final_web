<?php if ($lista){ ?>

	<div class="table-responsive">
  <table class="table table-hover">
		
		<thead>
			<tr>
				<th width="10%">Nº</th>
				<th width="30%">Nómina</th>
				<th width="10%">Foto</th>
				<th width="10%">T. Fijo</th>
				<th width="10%">T. Móvil</th>
				<th width="15%">Email</th>
				<th width="10%"></th>
			</tr>
				
		</thead>	
			<tbody>
				<?php $orden = 1; ?>
				<?php foreach ($lista as $dt){ ?>
				
				<tr>
					<td> <?php echo $orden; $orden++; ?></td>
					<td> <?php echo $dt->apellido. ' '.$dt->nombre; ?></td>
					<td></td>
					<td><?php echo $dt->telefono_fijo; ?></td>
					<td><?php echo $dt->telefono_movil; ?></td>
					<td><?php echo $dt->email; ?></td>
					<td>
						<button type="button" class="btn btn-info btn-sm" data-accion="editar" onclick="gestionRegistro(this);" data-id="<?php echo $dt->id; ?>"> <i class="fa fa-pencil"></i> </button>
						
						<button type="button" class="btn btn-danger btn-sm" data-accion="eliminar"onclick="gestionRegistro(this);" data-id="<?php echo $dt->id; ?>"> <i class="fa fa-trash"></i> </button>
					</td>
				</tr>
				<?php } ?>

			</tbody>
			     <tfoot>
			     	<tr>
			           <th >Nº</th>
				       <th >Nómina</th>
				       <th >Foto</th>
				       <th >T. Fijo</th>
				       <th >T. Móvil</th>
				       <th >Email</th>
				       <th ></th>
			     	</tr>
				     
			      </tfoot>
		
	 </table>
	</div>
<?php } else { ?>
	<div class="alert alert-danger"> 
      No existe datos
        </div>
<?php } ?>