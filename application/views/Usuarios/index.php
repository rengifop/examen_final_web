<script type="text/javascript">
  $("#menuOpciones").addClass("active");
  $("#menuUsuarios").addClass("active");
</script>
      <h1>
        <?php echo $tituloPagina; ?>
        <small>Gestion Usuario</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#"></a></li>
        <li class="active">Usuarios</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <button type="button" class="btn btn-success" data-accion="insertar" onclick="gestionRegistro(this);"><i class="fa fa-file"></i> Nuevo Usuario</button>
          </h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body" id="listadoDatos">
         
        </div>
        <!-- /.box-body -->
     </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!--Ventana modal-->
<div id="modalUsuario" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!--Modal content-->
      <div class="modal-content">

      <form id="formRegistro">
        <input type="hidden" name="idUsuario" id="idUsuario">
      <div class="modal-header">
        <h4 id="tituloModal"></h4>
      </div>

      <div class="modal-body">

        <div class="row">
          <div class="col-sm-6">
          <label>Apellido</label>
          <input type="text" name="apellidoUsuario" id="apellidoUsuario" class="form-control" placeholder="Apellidos">
          </div>
          <div class="col-sm-6">
          <label>Nombre</label>
          <input type="text" name="nombreUsuario" id="nombreUsuario" class="form-control" placeholder="Nombres">
          </div>
        </div>


        <div class="row">
          <div class="col-sm-12">
          <label>Email</label>  
           <div class="input-group">
             <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
             <input type="email" class="form-control" placeholder="@Email" name="emailUsuario" id="emailUsuario">
            </div>
           </div> 
        </div>


        <div class="row">
          <div class="col-sm-6">
           <label>Sexo</label>
           <div class="input-group">
           <span class="input-group-addon"><i class="fa fa-venus-mars"></i></span>
           <select name="sexoUsuario" id="sexoUsuario"  class="form-control">
             <option value="M">Masculino</option>
             <option value="F">Femenino</option>
           </select>
          </div> 
           </div>
            <div class="col-sm-6">
           <label>Estado</label>
           <div class="input-group">
           <span class="input-group-addon"><i class="fa fa-toggle-on"></i></span>
           <select name="estadoUsuario" id="estadoUsuario" class="form-control">
             <option value="A">Activo</option>
             <option value="I">Inactivo</option>
           </select>
         </div>
          </div> 
        </div>
         
           
         <br>
           <div class="row">
            <div class="col-sm-6">
                <label>Teléfono Fijo</label>
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                  <input type="number" name="telefonofijoUsuario" id="telefonofijoUsuario" class="form-control" placeholder="(_ _ _)_ _ _ - _ _ _ _"> 
                </div>
            </div>  
           
         
           
            <div class="col-sm-6">
                <label>Teléfono Móvil</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-mobile-phone"></i>
                  </div>
                  <input type="number" name="telefonomovilUsuario" id="telefonomovilUsuario" class="form-control" placeholder="(_ _ _)_ _ _ - _ _ _ _" > 
                </div>
            </div>  
           </div>
          
         <br>
           <div class="row">
            <div class="col-sm-6">
                <label>Usuario</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-user"></i>
                  </div>
                  <input type="text" class="form-control" placeholder="Usuario"data-inputmask='"mask": "(999) 999-9999"' data-mask> 
                </div>
            </div>  
           
         
           
            <div class="col-sm-6">
                <label>Clave</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-key"></i>
                  </div>
                  <input type="text" name="claveUsuario" id="claveUsuario" class="form-control" placeholder="Clave"data-inputmask='"mask": "(999) 999-9999"' data-mask> 
                </div>
            </div>  
           </div>
             
             <br>
            <div class="col-sm-6">
                <label>Foto</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-photo"></i>
                  </div>
                 <input type="file" name="fotoUsuario" id="fotoUsuario">
               </div>
             </div>
        
        <div class="row"> 
          <div class="col-sm-12">
          <label>Direccion</label>
          <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                <input type="direccion" class="form-control" placeholder="Dirección" name="direcionlUsuario" id="direcionUsuario">
              </div>
          </div>
        </div>


      </div>

      <div class="modal-footer">
        <button type="submit" id="botonGuardar" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
      </div>

      </form> 
    </div>
  </div>
</div>

  <script type="text/javascript">
    listarDatos();

    function listarDatos(){
      $("#listadoDatos").load("<?php echo base_url('Usuarios/lista') ?>",{},function(responseText, statusText, xhr){
        if (statusText == "success") {

        }
        if (statusText == "error") {

        }
      });
    }

    function gestionRegistro(objeto){
      var accion = $(objeto).data("accion");

      switch(accion){
        case 'insertar':
        $("#idUsuario").val("");
        $("#formRegistro")[0].reset();
        $("#modalUsuario").modal("show");
        $("#tituloModal").text("Insertar usuario");
        break;
        case 'editar':
        var id =$(objeto).data("id");
        editarRegistro(id);
        break;
        case 'eliminar':

        Swal.fire({
         title: 'Eliminar el registro?',
         text: "Se perderán los Datos!",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         cancelButtonText: 'No',
         confirmButtonText: 'Si!'
         }).then((result) => {
          if (result.value) {
           var id =$(objeto).data("id");
           $.ajax({
            type : "post",
            dataType : "json",
            url : "<?php echo base_url('Usuarios/eliminarRegistro') ?>",
            data:{
              id : id
            },
            success: function(data){
              Swal.fire({
           position: 'top-end',
            type: 'success',
             title: 'Usuario eliminado exitosamente',
             showConfirmButton: false,
             timer: 1500
              })
            },
            complete: function(){
              listarDatos();
            },
            error: function(err){
              Swal.fire({
           position: 'top-end',
            type: 'warning',
             title: 'No se pudo eliminar',
             showConfirmButton: false,
             timer: 1500
              })
            }
           });
          }
        })
        break;
      }

    }
    //VALIDACION DEL FORMULARIO

    $("#formRegistro").validate({
      rules :{
        apellidoUsuario : {
          required : true
        },
        nombreUsuario : {
          required : true
        },
        emailUsuario : {
          required: true,
          email: true
        }
      },
      messages : {
        apellidoUsuario : {
          required : "Ingrese el Apellido",
          field :"Ingrese solo letras"
        },
        nombreUsuario : {
          required : "Ingrese el Nombre",
          field :"Ingrese solo letras"
        },
        emailUsuario: {
           required : "Ingrese un correo",
          field :"El correo es incorrecto"
        }
      },
      submitHandler: function(form){
        var formData = new FormData($('#formRegistro')[0]);
        $.ajax({
          type : 'post',
          url : '<?php echo base_url("Usuarios/gestionRegistro"); ?>', 
          data : formData, 
          async : false,
          cache: false,
          contentType: false,
          processData: false,
          enctype: "multipar/form-data",
          success:function(data){
            var resultado = data.replace('"','');
            resultado = resultado.split("|");
            switch(resultado[0]){
              case 'i':
              var mensaje = "creado";
              break;
              case 'e':
              var mensaje ="editado";
              break;
            }
            //igual - done
            Swal.fire({
           position: 'top-end',
            type: 'success',
             title: 'Usuario ' +mensaje+ ' exitosamente',
             showConfirmButton: false,
             timer: 1500
              })
            $("#modalUsuario").modal("hide");
          },
          complete:function(){
            // igual a always
            listarDatos();
          },
          error:function(err){
            swal("Error!", "No se pudo cerar", "info");
          }
        })

      }
    });

    // EDITAR REGISTRO
    function editarRegistro(id){
      $.ajax({
        type : "post",
        url : "<?php echo base_url("Usuarios/editarRegistro"); ?>",
        dataType : "json",
        data : {
          id : id
        },
        success:function(data){ 
          $(data).each(function(i, v){
            $("#idUsuario").val(v.id);
            $("#apellidoUsuario").val(v.apellido);
            $("#nombreUsuario").val(v.nombre);
            $("#emailUsuario").val(v.email);
            $("#telefonomovilUsuario").val(v.telefono_movil);
            $("#telefono_fijoUsuario").val(v.telefono_fijo);
            $("#sexoUsuario").val(v.sexo);
            $("#estadoUsuario").val(v.estado);
            $("#usuarioUsuario").val(v.usuario);
            $("#claveUsuario").val(v.clave);
            //$("#fotoUsuario").val(v.foto);
            $("#direccionUsuario").val(v.direccion);
          });

         $("#modalUsuario").modal("show");
        $("#tituloModal").text("Editar usuario"); 
        },
        complete:function(){
          },
          error:function(err){
            Swal.fire({
               position: 'top-end',
               type: 'warning',
               title: 'Error al recuperar datos',
               showConfirmButton: false,
               timer: 1500
            })
          }
      });
    }
  </script>
  

  <!-- /.content-wrapper -->

  

  <!-- /.content-wrapper -->