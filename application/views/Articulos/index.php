
<script type="text/javascript">
  $("#menuRegistro").addClass("active");
  $("#menuArticulos").addClass("active");
</script>

      <h1>
        <?php echo $tituloPagina; ?>
        <small>Gestión de Articulos</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#"></a></li>
        <li class="active">Articulos</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
             <button type="button" class="btn btn-success" data-accion="insertar" onclick="gestionRegistro(this);"><i class="fa fa-file"></i> Nuevo registro</button> 
          </h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body" id="listadoDatos">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- Modal -->
<div id="modalFormulario" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
     
     <form id="formRegistro">

      <input type="hidden" name="idArticulo" id="idArticulo">

      <div class="modal-header">
        <h4 id="tituloModal"></h4>
      </div>

      <div class="modal-body">
        
        <br>
        <div class="row">
          <div class="col-sm-6">
            <label>Nombre Corto</label>
            <input type="text" name="nombreCortoArticulo" id="nombreCortoArticulo" class="form-control" placeholder="Nombre Corto">
          </div>
          <div class="col-sm-6">
            <label>Nombre Completo</label>
            <input type="text" name="nombreCompletoArticulo" id="nombreCompletoArticulo" class="form-control" placeholder="Nombre Completo">
          </div>
        </div> 

           <br>
         <div class="row">
          <div class="col-sm-6">
            <label>Codigo de Barra:</label>
            <input type="text" name="codigoBarrasArticulo" id="codigoBarrasArticulo" class="form-control" placeholder="Codigo">
          </div>
          <div class="col-sm-6">
            <label>Fecha Ingreso</label>
            <div class="input-group">
             <span class="input-group-addon"><i class="fa fa-calendar "></i></span>
            <input type="text" name="fechaIngresoArticulo" id="fechaIngresoArticulo" class="form-control" placeholder="Fecha" value="<?php echo date("Y-m-d");?>">
          </div>
          </div>
           </div>

            <br>
          <div class="row">
          <div class="col-sm-6">
            <label>Estado:</label>
            <div class="input-group">
             <span class="input-group-addon"><i class=" "></i></span>
            <select name="estadoArticulo" id="estadoArticulo"  class="form-control">
                <option value="A">Activo</option>
                <option value="R">Retirado</option>
                </select>
          </div>
        </div> 
        <div class="col-sm-6">
            <label>Tipo:</label>
            <div class="input-group">
             <span class="input-group-addon"><i class=" "></i></span>
            <select name="tipoArticulo" id="tipoArticulo"  class="form-control">
                <option value="P">Productos</option>
                <option value="S">servicios</option>
                </select>
          </div>
        </div>
        </div>

           
           <br>
          <div class="row">
          <div class="col-sm-6">
            <label>Unidad:</label>
            <div class="input-group">
             <span class="input-group-addon"><i class=" "></i></span>
            <select name="unidadArticulo" id="unidadArticulo"  class="form-control">
                <option value="Lb">Libra</option>
                <option value="Lt">Litro</option>
                <option value="U">Unidad</option>
                </select>
          </div>
        </div>
        </div>
            

            <br>
          <div class="row">
          <div class="col-sm-4">
            <label>Precio Compra Articulo:</label>
            <div class="input-group">
             <span class="input-group-addon"><i class=" "></i></span>
            <input type="text" name="precioCompraArticulo" id="precioCompraArticulo" class="form-control" placeholder="Precio">
          </div>
        </div>
          <div class="col-sm-4">
            <label>Precio Dist. Articulo:</label>
            <div class="input-group">
             <span class="input-group-addon"><i class="fa fa-money "></i></span>
            <input type="text" name="precioDistribuidorArticulo" id="precioDistribuidorArticulo" class="form-control" placeholder="Precio">
          </div>
          </div>

          <div class="col-sm-4">
            <label>Precio Publico:</label>
            <div class="input-group">
             <span class="input-group-addon"><i class=" "></i></span>
            <input type="text" name="precioPublicoArticulo" id="precioPublicoArticulo" class="form-control" placeholder="Precio">
          </div>
          </div>
        </div>

          <br>
          <div class="row"> 
          <div class="col-sm-8">
            <label>Foto:</label>
            <input type="file" name="fotoArticulo" id="fotoArticulo">
          </div>
        </div>
          
         
    
          <br>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
     </form>

    </div>
  </div>
</div>


  <script type="text/javascript">
    
    listarDatos();

    function listarDatos(){
      
      $("#listadoDatos").load("<?php echo base_url('Articulos/lista') ?>",{},function(responseText, statusText, xhr){
        if(statusText == "success"){
        }
        if(statusText == "error"){
        }
      });
    
    }

    function gestionRegistro(objeto){
      var accion = $(objeto).data("accion");
      switch(accion){
        case 'insertar':
          $("#idArticulo").val("");
          $("#formRegistro")[0].reset();
          $("#modalFormulario").modal("show");
          $("#tituloModal").text("Insertar registro");
        break;
        case 'editar':
          var id = $(objeto).data("id");
          editarRegistro(id);
        break;
        case 'eliminar':

          Swal.fire({
            title: 'Eliminar el registro?',
            text: "Se perderán los datos!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'No'
          }).then((result) => {
            if (result.value) {

              var id = $(objeto).data("id"); 
              $.ajax({
                type : "post",
                dataType: "json",
                url: "<?php echo base_url('Articulos/eliminarRegistro'); ?>",
                data: {
                  id : id
                },
                success: function(data){
                  Swal.fire({
                    position: 'top-end',
                    type: 'success',
                    title: 'Registro eliminado exitosamente',
                    showConfirmButton: false,
                    timer: 1500
                  })
                },
                complete:function(){
                  listarDatos();
                },
                error:function(err){
                  Swal.fire({
                    position: 'top-end',
                    type: 'warning',
                    title: 'No se pudo eliminar',
                    showConfirmButton: false,
                    timer: 1500
                  })
                } 
              });

            }
          })

        break;
      } 
    }

    //Validacion de formulario

    $("#formRegistro").validate({
      rules : {
        nombreMarca : {
          required : true
        },
        origenMarca : {
          required : true
        }
      },
      messages : {
        nombreMarca : {
          required : "Ingrese un nombre"
        },
        origenMarca : {
          required : "País de origen"
        }
      },
      submitHandler: function(form) {
        var formData = new FormData($('#formRegistro')[0]);
        $.ajax({
          type : "post",
          url : "<?php echo base_url('Articulos/gestionRegistro'); ?>",
          data : formData,
          async: false,
          cache: false,
          contentType: false,
          processData: false,
          enctype: 'multipart/form-data',
          success:function(data){
            var resultado = data.replace('"', '');
            resultado = resultado.split("|");
            switch(resultado[0]){
              case 'i':
                var mensaje = "creado";
              break;
              case 'e':
                var mensaje = "editado";
              break;
            }
            //igual - done
            Swal.fire({
              position: 'top-end',
              type: 'success',
              title: 'Registro '+mensaje+' exitosamente',
              showConfirmButton: false,
              timer: 1500
            })
            $("#modalFormulario").modal("hide");
          },
          complete:function(){
            // igual a always
            listarDatos();
          },
          error:function(err){
             
          }   
        });
      }
    });

    //Editar registro
    function editarRegistro(id){
      $.ajax({
        type : "post",
        url : "<?php echo base_url('Articulos/editarRegistro'); ?>",
        dataType : 'json',
        data : {
          id : id
        },
        success: function(data){
          $(data).each(function(i, v){
            $("#idArticulo").val(v.id);
            $("#nombreCompletoArticulo").val(v.nombre_completo);
            $("#codigoBarrasArticulo").val(v.codigo_barra);
            $("#nombreCortoArticulo").val(v.nombre_corto);
            $("#fechaIngresoArticulo").val(v.fecha_ingreso);
            $("#estadoArticulo").val(v.estado);
            $("#unidadArticulo").val(v.unidad);
            $("#precioCompraArticulo").val(v.precio_compra);
            $("#precioDistribuidorArticulo").val(v.precio_distribuidor);
            $("#precioPublicoArticulo").val(v.precio_publico);
            $("#tipoArticulo").val(v.tipo);
          });

          $("#modalFormulario").modal("show");
          $("#tituloModal").text("Editar registro");
          
        },
        complete:function(){
        },
        error:function(err){
            Swal.fire({
              position: 'top-end',
              type: 'warning',
              title: 'Error al recuperar datos',
              showConfirmButton: false,
              timer: 1500
            })
        }
      });
    }

  </script>