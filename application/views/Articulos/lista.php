<?php if($lista){ ?>

<div class="table-responsive">	
  <table class="table table-bordered">
	<thead>
		<tr>
			<th width="5%">N°</th>
			<th width="10%">Nombre Completo</th>
			<th width="10%">Codigo barra </th>
			<th width="10%">Fecha ingreso</th>
			<th width="10%">Precio Compra</th>
			<th width="5%"></th>
		</tr>
	</thead>
	<tbody>
		<?php $orden = 1; ?>
		<?php foreach ($lista as $dt){ ?>
		<tr>
			<td><?php echo $orden; $orden++; ?></td>
			<td><?php echo $dt->nombre_completo; ?></td>
			<td><?php echo $dt->codigo_barra; ?></td>
			<td><?php echo $dt->fecha_ingreso; ?></td>
			<td><?php echo $dt->precio_compra; ?></td>

			<td>
				<button type="button" class="btn btn-info btn-sm" data-accion="editar" onclick="gestionRegistro(this);" data-id="<?php echo $dt->id; ?>"><i class="fa fa-pencil"></i></button>
				<button type="button" class="btn btn-danger btn-sm" data-accion="eliminar" onclick="gestionRegistro(this);"data-id="<?php echo $dt->id; ?>"><i class="fa fa-trash"></i></button>
			</td>
		</tr>
		<?php } ?>
	</tbody>
	<tfoot>
		<tr>
			
			<th >N°</th>
			<th >Nombre Completo</th>
			<th >Codigo barra </th>
			<th >Fecha ingreso</th>
			<th >Precio Compra</th>
			<th ></th>
		</tr>		
	</tfoot>	
  </table>

</div>
<?php }else{ ?>
  <div class="alert alert-danger">
       No existen datos
  </div>
<?php } ?>