 <script type="text/javascript">
  $("#menuEscuela").addClass("active");
  $("#menuProfesor").addClass("active");
</script>

      <h1>
        <?php echo $tituloPagina; ?>
        <small>Gestión de Materias</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#"></a></li>
        <li class="active">Profesores</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
             <button type="button" class="btn btn-success" data-accion="insertar" onclick="gestionRegistro(this);"><i class="fa fa-file"></i> Nuevo Registro</button> 
          </h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body" id="listadoDatos">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- Modal -->
<div id="modalFormulario" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
     
     <form id="formRegistro">

      <input type="hidden" name="idProfesor" id="idProfesor">

      <div class="modal-header">
        <h4 id="tituloModal"></h4>
      </div>

      <div class="modal-body">
        
        <div class="row">
          <div class="col-sm-6">
            <label>Nombre</label>
            <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user-o"></i></span>
            <input type="text" name="nombreProfesor" id="nombreProfesor" class="form-control" placeholder="Nombre">
          </div>
          </div>

          <div class="col-sm-6">
            <label>Apellido</label>
            <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user-o"></i></span>
            <input type="text" name="apellidoProfesor" id="apellidoProfesor" class="form-control" placeholder="Apellido">
          </div>
          </div>
        </div> 


          <br>
        <div class="row">
          <div class="col-sm-6">
           <label>Sexo</label>
           <div class="input-group">
           <span class="input-group-addon"><i class="fa fa-venus-mars"></i></span>
           <select name="sexoProfesor" id="sexoProfesor"  class="form-control">
             <option value="M">Masculino</option>
             <option value="F">Femenino</option>
           </select>
          </div> 
           </div>

          <div class="col-sm-6">
                <label>Celular</label>
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-mobile-phone"></i></div>
                  <input type="text" name="celularProfesor" id="celularProfesor" class="form-control" placeholder="(_ _ _)_ _ _ - _ _ _ _"> 
                </div>
            </div>  
           </div>
      
      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
     </form>

    </div>
  </div>
</div>


  <script type="text/javascript">
    
    listarDatos();

    function listarDatos(){
      
      $("#listadoDatos").load("<?php echo base_url('Profesores/lista') ?>",{},function(responseText, statusText, xhr){
        if(statusText == "success"){
        }
        if(statusText == "error"){
        }
      });
    
    }

    function gestionRegistro(objeto){
      var accion = $(objeto).data("accion");
      switch(accion){
        case 'insertar':
          $("#idProfesor").val("");
          $("#formRegistro")[0].reset();
          $("#modalFormulario").modal("show");
          $("#tituloModal").text("Insertar registro");
        break;
        case 'editar':
          var id = $(objeto).data("id");
          editarRegistro(id);
        break;
        case 'eliminar':

          Swal.fire({
            title: 'Eliminar el registro?',
            text: "Se perderán los datos!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'No'
          }).then((result) => {
            if (result.value) {

              var id = $(objeto).data("id"); 
              $.ajax({
                type : "post",
                dataType: "json",
                url: "<?php echo base_url('Profesores/eliminarRegistro'); ?>",
                data: {
                  id : id
                },
                success: function(data){
                  Swal.fire({
                    position: 'top-end',
                    type: 'success',
                    title: 'Registro eliminado exitosamente',
                    showConfirmButton: false,
                    timer: 1500
                  })
                },
                complete:function(){
                  listarDatos();
                },
                error:function(err){
                  Swal.fire({
                    position: 'top-end',
                    type: 'warning',
                    title: 'No se pudo eliminar',
                    showConfirmButton: false,
                    timer: 1500
                  })
                } 
              });

            }
          })

        break;
      } 
    }

    //Validacion de formulario

    $("#formRegistro").validate({
      rules : {
        nombreProfesor : {
          required : true
        }
      },
      messages : {
        nombreProfesor : {
          required : "Ingrese un nombre"
        }
      },
      submitHandler: function(form) {
        var formData = new FormData($('#formRegistro')[0]);
        $.ajax({
          type : "post",
          url : "<?php echo base_url('Profesores/gestionRegistro'); ?>",
          data : formData,
          async: false,
          cache: false,
          contentType: false,
          processData: false,
          enctype: 'multipart/form-data',
          success:function(data){
            var resultado = data.replace('"', '');
            resultado = resultado.split("|");
            switch(resultado[0]){
              case 'i':
                var mensaje = "creado";
              break;
              case 'e':
                var mensaje = "editado";
              break;
            }
            //igual - done
            Swal.fire({
              position: 'top-end',
              type: 'success',
              title: 'Registro '+mensaje+' exitosamente',
              showConfirmButton: false,
              timer: 1500
            })
            $("#modalFormulario").modal("hide");
          },
          complete:function(){
            // igual a always
            listarDatos();
          },
          error:function(err){
             
          }   
        });
      }
    });

    //Editar registro
    function editarRegistro(id){
      $.ajax({
        type : "post",
        url : "<?php echo base_url('Profesores/editarRegistro'); ?>",
        dataType : 'json',
        data : {
          id : id
        },
        success: function(data){
          $(data).each(function(i, v){
            $("#idProfesor").val(v.id);
            $("#nombreProfesor").val(v.nombre);
          });

          $("#modalFormulario").modal("show");
          $("#tituloModal").text("Editar registro");
          
        },
        complete:function(){
        },
        error:function(err){
            Swal.fire({
              position: 'top-end',
              type: 'warning',
              title: 'Error al recuperar datos',
              showConfirmButton: false,
              timer: 1500
            })
        }
      });
    }

  </script>