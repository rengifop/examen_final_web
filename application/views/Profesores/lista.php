<?php if($lista){ ?>

<div class="table-responsive">	
  <table class="table table-bordered">
	<thead>
		<tr>
			<th width="10%">N°</th>
			<th width="50%">Nombre</th>
			<th width="12%"></th>
		</tr>
	</thead>
	<tbody>
		<?php $orden = 1; ?>
		<?php foreach ($lista as $dt){ ?>
		<tr>
			<td><?php echo $orden; $orden++; ?></td>
			<td><?php echo $dt->nombre; ?></td>
			<td>
				<button type="button" class="btn btn-info btn-sm" data-accion="editar" onclick="gestionRegistro(this);" data-id="<?php echo $dt->id; ?>"><i class="fa fa-pencil"></i></button>
				<button type="button" class="btn btn-danger btn-sm" data-accion="eliminar" onclick="gestionRegistro(this);"data-id="<?php echo $dt->id; ?>"><i class="fa fa-trash"></i></button>
			</td>
		</tr>
		<?php } ?>
	</tbody>
	<tfoot>
		<tr>
			<th>N°</th>
			<th>Nombre</th>
			<th></th>
		</tr>		
	</tfoot>	
  </table>

</div>
<?php }else{ ?>
  <div class="alert alert-danger">
       No existen datos
  </div>
<?php } ?>