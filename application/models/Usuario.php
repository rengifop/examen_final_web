<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * // CREADO POR PATRICIO RENGIFO
 */
class Usuario extends CI_Model{
	public function buscarUsuarioPorEmail($emailUsuario){
		$sql = "SELECT usuario.id AS idUsuario, usuario.nombre AS nombreUsuario, usuario.apellido AS apellidoUsuario, usuario.clave AS claveUsuario, usuario.sexo AS sexoUsuario, usuario.estado AS estadoUsuario, usuario.email AS emailUsuario, usuario.direccion AS direccionUsuario, usuario.telefono_fijo AS telefono_fijoUsuario, usuario.telefono_movil AS telefono_movilUsuario, usuario.foto AS fotoUsuario FROM usuario WHERE usuario.estado = 'A' AND usuario.email = '$emailUsuario'";
		// EJECUTAR SQL
		 $resultado = $this->db->query($sql);

		//$this->db->("*");
		//$this->db->where("email",$emailUsuario);
		//$resultado = $this->db->get("usuario");
		 if($resultado->num_rows() > 0){
		 	return $resultado->row();
		 }
		 else{
		 	return false;
		 }
	}
	public function buscarUsuarios(){
		$this->db->select("*");
		$resultado = $this->db->get("usuario");
		if ($resultado->num_rows() > 0) {
			return $resultado->result();
		}
		else{
			return false;
		}
	}

	public function insertarRegistro($data){
		$this->db->insert('usuario', $data);
		return $this->db->insert_id();			
	}

	public function editarRegistro($idUsuario, $data){
		$this->db->where('id', $idUsuario);
		$this->db->update('usuario', $data);
		return $this->db->affected_rows();			
	}
    public function eliminarRegistro($idUsuario){
    	$this->db->where("id",$idUsuario);
    	$this->db->delete("usuario");
    	return $this->db->affected_rows();

    }
    public function buscarRegistroPorID($idUsuario){
    	$this->db->select("*");
    	$this->db->where("id",$idUsuario);
    	$resultado = $this->db->get("usuario");
    	if($resultado->num_rows() > 0){
    		return $resultado->row();
    	}
    	else{
    		return false;
    	}
    }
    
}

?>