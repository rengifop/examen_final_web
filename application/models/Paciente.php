<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Paciente extends CI_model{

	public function insertarRegistro($data){
		$this->db->insert('pacientes', $data);
		return $this->db->insert_id();
	}

	public function editarRegistro($idPaciente, $data){
		$this->db->where('id', $idPaciente);
		$this->db->update('pacientes', $data);
		return $this->db->affected_rows();
	}
	
	public function buscarPacientes(){
		$sql = "SELECT pacientes.id AS idPaciente, pacientes.nombre AS nombrePaciente, pacientes.apellido AS apellidoPaciente, pacientes.direccion AS direccionPaciente, pacientes.telefono_celular AS telefonoCelularPaciente, pacientes.edad AS edadPaciente, pacientes.hospital_id AS idHospital FROM pacientes INNER JOIN hospital ON pacientes.hospital_id = hospital.id ORDER BY nombrePaciente ";
		$resultado = $this->db->query($sql);

		if($resultado->num_rows() > 0){
			return $resultado->result();
		}
		else{
			return false;
		}
	}

	public function buscarRegistroPorID($idPaciente){
		$sql = "SELECT pacientes.id AS idPaciente, pacientes.nombre AS nombrePaciente, pacientes.apellido AS apellidoPaciente, pacientes.direccion AS direccionPaciente, pacientes.telefono_celular AS telefonoCelularPaciente, pacientes.edad AS edadPaciente, pacientes.hospital_id AS idHospital FROM pacientes INNER JOIN hospital ON pacientes.hospital_id = hospital.id WHERE pacientes.id = $idPaciente  ORDER BY nombrePaciente ASC";
		$resultado = $this->db->query($sql);
		if($resultado->num_rows() > 0){
			return $resultado->row();
		}
		else{
			return false;
		}
	}

	public function eliminarRegistro($idPaciente){
		$this->db->where("id", $idPaciente);
		$this->db->delete("pacientes");
		return $this->db->affected_rows();	
	}


}