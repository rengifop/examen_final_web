<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Articulo extends CI_model{

	public function insertarRegistro($data){
		$this->db->insert('articulos', $data);
		return $this->db->insert_id();
	}

	public function editarRegistro($idUsuario, $data){
		$this->db->where('id', $idUsuario);
		$this->db->update('articulos', $data);
		return $this->db->affected_rows();
	}
	
	public function buscarArticulos(){
		$this->db->select("*");
		$resultado = $this->db->get("articulos");

		if($resultado->num_rows() > 0){
			return $resultado->result();
		}
		else{
			return false;
		}
	}

	public function buscarRegistroPorID($idArticulo){
		$this->db->select("*");
		$this->db->where("id", $idArticulo);
		$resultado = $this->db->get("articulos");
		if($resultado->num_rows() > 0){
			return $resultado->row();
		}
		else{
			return false;
		}
	}

	public function eliminarRegistro($idArticulo){
		$this->db->where("id", $idArticulo);
		$this->db->delete("articulos");
		return $this->db->affected_rows();	
	}


}