<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * // CREADO POR PATRICIO RENGIFO
 */
class Cliente extends CI_Model{
	public function buscarClientePorEmail($correo_personalCliente){
		$sql = "SELECT cliente.id AS idCliente, cliente.nombre AS nombreCliente, cliente.apellido AS apellidoCliente, cliente.tipo_identificacion AS tipo_identificacionCliente, cliente.numero_identificacion AS numero_identificacionCliente, cliente.fecha_nacimiento AS fecha_nacimientoCliente, cliente.fecha_registro AS fecha_resgistroCliente, cliente.correo_personal AS correo_personalCliente, cliente.correo_corporativo AS correo_corporativoCliente, cliente.sexo AS sexoCliente, cliente.religion AS religionCliente, cliente.estado_civil AS estado_civilCliente, cliente.etnia AS etniaCliente, cliente.tipo_cliente AS tipo_clienteCliente FROM cliente WHERE cliente.correo_personal = '$correo_personalCliente'";
		// EJECUTAR SQL
		 $resultado = $this->db->query($sql);

		//$this->db->("*");
		//$this->db->where("correo_personal",$correo_personalCliente);
		//$resultado = $this->db->get("cliente");
		 if($resultado->num_rows() > 0){
		 	return $resultado->row();
		 }
		 else{
		 	return false;
		 }
	}
	public function buscarClientes(){
		$this->db->select("*");
		$resultado = $this->db->get("cliente");
		if ($resultado->num_rows() > 0) {
			return $resultado->result();
		}
		else{
			return false;
		}
	}

	public function insertarRegistro($data){
		$this->db->insert('cliente', $data);
		return $this->db->insert_id();			
	}

	public function editarRegistro($idCliente, $data){
		$this->db->where('id', $idCliente);
		$this->db->update('cliente', $data);
		return $this->db->affected_rows();			
	}
    public function eliminarRegistro($idCliente){
    	$this->db->where("id",$idCliente);
    	$this->db->delete("cliente");
    	return $this->db->affected_rows();

    }
    public function buscarRegistroPorID($idCliente){
    	$this->db->select("*");
    	$this->db->where("id",$idCliente);
    	$resultado = $this->db->get("cliente");
    	if($resultado->num_rows() > 0){
    		return $resultado->row();
    	}
    	else{
    		return false;
    	}
    }
    
}

?>