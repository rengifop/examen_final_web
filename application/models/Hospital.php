<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Hospital extends CI_model{

	public function insertarRegistro($data){
		$this->db->insert('hospital', $data);
		return $this->db->insert_id();
	}

	public function editarRegistro($idUsuario, $data){
		$this->db->where('id', $idUsuario);
		$this->db->update('hospital', $data);
		return $this->db->affected_rows();
	}
	
	public function buscarHospital(){
		$this->db->select("*");
		$resultado = $this->db->get("hospital");

		if($resultado->num_rows() > 0){
			return $resultado->result();
		}
		else{
			return false;
		}
	}

	public function buscarRegistroPorID($idHospital){
		$this->db->select("*");
		$this->db->where("id", $idHospital);
		$resultado = $this->db->get("hospital");
		if($resultado->num_rows() > 0){
			return $resultado->row();
		}
		else{
			return false;
		}
	}

	public function eliminarRegistro($idHospital){
		$this->db->where("id", $idHospital);
		$this->db->delete("hospital");
		return $this->db->affected_rows();	
	}


}