<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Ingreso extends CI_model{

	public function insertarRegistro($data){
		$this->db->insert('ingreso', $data);
		return $this->db->insert_id();
	}

	public function editarRegistro($idIngreso, $data){
		$this->db->where('id', $idIngreso);
		$this->db->update('ingreso', $data);
		return $this->db->affected_rows();
	}
	
	public function buscarIngresos(){
		$sql = "SELECT ingreso.id AS idIngreso, ingreso.fecha_ingreso AS fechaIngreso, ingreso.detalle AS detalleIngreso, ingreso.cantidad_ingreso AS cantidadIngreso, ingreso.precio_ingreso AS precioIngreso, ingreso.numero_factura AS numeroFacturaIngreso, ingreso.articulos_id AS idArticulos FROM ingreso INNER JOIN articulos ON ingreso.articulos_id = articulos.id ORDER BY fechaIngreso ASC";
		$resultado = $this->db->query($sql);

		if($resultado->num_rows() > 0){
			return $resultado->result();
		}
		else{
			return false;
		}
	}

	public function buscarRegistroPorID($idIngreso){
		$sql = "SELECT ingreso.id AS idIngreso, ingreso.fecha_ingreso AS fechaIngreso, ingreso.detalle AS detalleIngreso, ingreso.cantidad_ingreso AS cantidadIngreso, ingreso.precio_ingreso AS precioIngreso, ingreso.numero_factura AS numeroFacturaIngreso, ingreso.articulos_id AS idArticulos FROM ingreso INNER JOIN articulos ON ingreso.articulos_id = articulos.id WHERE ingreso.id = $idIngreso ORDER BY fechaIngreso ASC";

		$resultado = $this->db->query($sql);
		if($resultado->num_rows() > 0){
			return $resultado->row();
		}
		else{
			return false;
		}
	}

	public function eliminarRegistro($idIngreso){
		$this->db->where("id", $idIngreso);
		$this->db->delete("ingreso");
		return $this->db->affected_rows();	
	}


}