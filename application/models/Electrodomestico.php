<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Electrodomestico extends CI_model{

	public function insertarRegistro($data){
		$this->db->insert('electrodomestico', $data);
		return $this->db->insert_id();
	}

	public function editarRegistro($idElectrodomestico, $data){
		$this->db->where('id', $idElectrodomestico);
		$this->db->update('electrodomestico', $data);
		return $this->db->affected_rows();
	}
	
	public function buscarElectrodomesticos(){
		$sql = "SELECT electrodomestico.id AS idElectrodomestico, electrodomestico.marca_id AS idMarca, electrodomestico.nombre AS nombreElectodomestico, electrodomestico.fabricacion AS fabricacionElectrodomestico, electrodomestico.estado AS estadoElectrodomestico, electrodomestico.color AS coloElectrodomestico, marca.nombre AS nombreMarca FROM electrodomestico INNER JOIN marca ON electrodomestico.marca_id = marca.id ORDER BY nombreMarca ASC, estadoElectrodomestico ASC";
		$resultado = $this->db->query($sql);

		if($resultado->num_rows() > 0){
			return $resultado->result();
		}
		else{
			return false;
		}
	}

	public function buscarRegistroPorID($idElectrodomestico){
		$sql = "SELECT electrodomestico.id AS idElectrodomestico, electrodomestico.marca_id AS idMarca, electrodomestico.nombre AS nombreElectrodomestico, electrodomestico.fabricacion AS fabricacionElectrodomestico, electrodomestico.estado AS estadoElectrodomestico, electrodomestico.color AS colorElectrodomestico, marca.nombre AS nombreMarca FROM electrodomestico INNER JOIN marca ON electrodomestico.marca_id = marca.id WHERE electrodomestico.id = $idElectrodomestico  ORDER BY nombreMarca ASC, estadoElectrodomestico ASC";
		$resultado = $this->db->query($sql);
		if($resultado->num_rows() > 0){
			return $resultado->row();
		}
		else{
			return false;
		}
	}

	public function eliminarRegistro($idElectrodomestico){
		$this->db->where("id", $idElectrodomestico);
		$this->db->delete("electrodomestico");
		return $this->db->affected_rows();	
	}


}