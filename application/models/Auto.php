<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Auto extends CI_model{

	public function insertarRegistro($data){
		$this->db->insert('auto', $data);
		return $this->db->insert_id();
	}

	public function editarRegistro($idAuto, $data){
		$this->db->where('id', $idAuto);
		$this->db->update('auto', $data);
		return $this->db->affected_rows();
	}
	
	public function buscarAutos(){
		$sql = "SELECT auto.id AS idAuto, auto.marca_id AS idMarca, auto.nombre AS nombreAuto, auto.fabricacion AS fabricacionAuto, auto.tipo AS tipoAuto, auto.estado AS estadoAuto, auto.color AS coloAuto, auto.puerta AS puertaAuto, marca.nombre AS nombreMarca FROM auto INNER JOIN marca ON auto.marca_id = marca.id ORDER BY nombreMarca ASC, tipoAuto ASC";
		$resultado = $this->db->query($sql);

		if($resultado->num_rows() > 0){
			return $resultado->result();
		}
		else{
			return false;
		}
	}

	public function buscarRegistroPorID($idAuto){
		$sql = "SELECT auto.id AS idAuto, auto.marca_id AS idMarca, auto.nombre AS nombreAuto, auto.fabricacion AS fabricacionAuto, auto.tipo AS tipoAuto, auto.estado AS estadoAuto, auto.color AS colorAuto, auto.puerta AS puertaAuto, marca.nombre AS nombreMarca FROM auto INNER JOIN marca ON auto.marca_id = marca.id WHERE auto.id = $idAuto  ORDER BY nombreMarca ASC, tipoAuto ASC";
		$resultado = $this->db->query($sql);
		if($resultado->num_rows() > 0){
			return $resultado->row();
		}
		else{
			return false;
		}
	}

	public function eliminarRegistro($idAuto){
		$this->db->where("id", $idAuto);
		$this->db->delete("auto");
		return $this->db->affected_rows();	
	}


}