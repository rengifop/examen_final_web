<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Profesor extends CI_model{

	public function insertarRegistro($data){
		$this->db->insert('profesor', $data);
		return $this->db->insert_id();
	}

	public function editarRegistro($idUsuario, $data){
		$this->db->where('id', $idUsuario);
		$this->db->update('profesor', $data);
		return $this->db->affected_rows();
	}
	
	public function buscarProfesor(){
		$this->db->select("*");
		$resultado = $this->db->get("profesor");

		if($resultado->num_rows() > 0){
			return $resultado->result();
		}
		else{
			return false;
		}
	}

	public function buscarRegistroPorID($idProfesor){
		$this->db->select("*");
		$this->db->where("id", $idProfesor);
		$resultado = $this->db->get("profesor");
		if($resultado->num_rows() > 0){
			return $resultado->row();
		}
		else{
			return false;
		}
	}

	public function eliminarRegistro($idProfesor){
		$this->db->where("id", $idProfesor);
		$this->db->delete("profesor");
		return $this->db->affected_rows();	
	}


}