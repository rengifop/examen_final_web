<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Marca extends CI_model{

	public function insertarRegistro($data){
		$this->db->insert('marca', $data);
		return $this->db->insert_id();
	}

	public function editarRegistro($idUsuario, $data){
		$this->db->where('id', $idUsuario);
		$this->db->update('marca', $data);
		return $this->db->affected_rows();
	}
	
	public function buscarMarcas(){
		$this->db->select("*");
		$resultado = $this->db->get("marca");

		if($resultado->num_rows() > 0){
			return $resultado->result();
		}
		else{
			return false;
		}
	}

	public function buscarRegistroPorID($idMarca){
		$this->db->select("*");
		$this->db->where("id", $idMarca);
		$resultado = $this->db->get("marca");
		if($resultado->num_rows() > 0){
			return $resultado->row();
		}
		else{
			return false;
		}
	}

	public function eliminarRegistro($idMarca){
		$this->db->where("id", $idMarca);
		$this->db->delete("marca");
		return $this->db->affected_rows();	
	}


}