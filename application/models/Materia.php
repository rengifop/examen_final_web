<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Materia extends CI_model{

	public function insertarRegistro($data){
		$this->db->insert('materia', $data);
		return $this->db->insert_id();
	}

	public function editarRegistro($idMateria, $data){
		$this->db->where('id', $idMateria);
		$this->db->update('materia', $data);
		return $this->db->affected_rows();
	}
	
	public function buscarMaterias(){
		$sql = "SELECT materia.id AS idMateria, materia.nombre AS nombreMateria, materia.temas AS temasMateria, materia.profesor_id AS idProfesor, materia.edicion AS edicionMateria FROM materia INNER JOIN profesor ON materia.profesor_id = profesor.id ORDER BY nombreMateria ";
		$resultado = $this->db->query($sql);

		if($resultado->num_rows() > 0){
			return $resultado->result();
		}
		else{
			return false;
		}
	}

	public function buscarRegistroPorID($idMateria){
		$sql = "SELECT materia.id AS idMateria, materia.nombre AS nombreMateria, materia.temas AS temasMateria, materia.profesor_id AS idProfesor, materia.edicion AS edicionMateria FROM materia INNER JOIN profesor ON materia.profesor_id = profesor.id WHERE materia.id = $idMateria  ORDER BY nombreMateria ASC";
		$resultado = $this->db->query($sql);
		if($resultado->num_rows() > 0){
			return $resultado->row();
		}
		else{
			return false;
		}
	}

	public function eliminarRegistro($idMateria){
		$this->db->where("id", $idMateria);
		$this->db->delete("materia");
		return $this->db->affected_rows();	
	}


}